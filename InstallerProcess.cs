﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary
{
    public partial class InstallerProcess : Form
    {
        public InstallerProcess()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
            this.progressBar1.Visible = true;
            this.progressBar1.PerformStep();
            this.label1.Text = "正在安装 Sqlite 驱动程序";
            this.btnStart.Visible = false;
            this.btnFinish.Enabled = false;
            this.btnFinish.Visible = true;

            //创建启动对象
            System.Diagnostics.Process exep = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName = Application.StartupPath + @"\Sqlite\sqliteodbc.exe";

            //设置启动动作,确保以管理员身份运行
            startInfo.Verb = "runas";
            exep.StartInfo = startInfo;

            try
            {
                exep.Start();
                //exep.WaitForExit();
            }
            catch
            {
                return;
            }
        }

        private void InstallerProcess_Load(object sender, EventArgs e)
        {
            this.btnFinish.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CommonFunctions.CheckInstalledSqliteDriver())
            {
                timer1.Stop();
                timer1.Enabled = false;
                this.btnStart.Visible = false;
                this.btnFinish.Enabled = true;
                this.btnCancel.Visible = false;
                this.progressBar1.Visible = false;
                this.label1.Text = "您已经准备好环境，点击【完成】按钮便可使用葡萄城报表模板库！";
            }
            else
            {
                this.progressBar1.Value = (this.progressBar1.Value + 10) % 100;
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void InstallerProcess_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
