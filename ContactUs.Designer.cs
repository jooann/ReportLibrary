﻿namespace GrapecityReportsLibrary
{
    partial class ContactUs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactUs));
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.linkButtonControl1 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.linkButtonControl2 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(37, 383);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "QQ群：";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(23, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(849, 303);
            this.label7.TabIndex = 6;
            this.label7.Text = resources.GetString("label7.Text");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(15, 411);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 22);
            this.label8.TabIndex = 7;
            this.label8.Text = "产品官网：";
            // 
            // linkButtonControl1
            // 
            this.linkButtonControl1.ButtonBackColor = System.Drawing.Color.Empty;
            this.linkButtonControl1.ButtonText = "ActiveReports报表开发控件";
            this.linkButtonControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl1.EventData = null;
            this.linkButtonControl1.EventName = null;
            this.linkButtonControl1.EventType = null;
            this.linkButtonControl1.EventValue = null;
            this.linkButtonControl1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkButtonControl1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.linkButtonControl1.LinkUrl = "http://www.gcpowertools.com.cn/products/activereports_overview.htm?utm_source=Dem" +
    "o&utm_medium=ActiveReports&utm_term=GrapecityReportsLibrary&utm_content=arwebsit" +
    "e";
            this.linkButtonControl1.Location = new System.Drawing.Point(103, 411);
            this.linkButtonControl1.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl1.Name = "linkButtonControl1";
            this.linkButtonControl1.Size = new System.Drawing.Size(225, 26);
            this.linkButtonControl1.TabIndex = 8;
            this.linkButtonControl1.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl1_ButtonClick);
            this.linkButtonControl1.Load += new System.EventHandler(this.linkButtonControl1_Load);
            // 
            // linkButtonControl2
            // 
            this.linkButtonControl2.ButtonBackColor = System.Drawing.Color.Empty;
            this.linkButtonControl2.ButtonText = "109783140（点击直接加群）";
            this.linkButtonControl2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl2.EventData = null;
            this.linkButtonControl2.EventName = null;
            this.linkButtonControl2.EventType = null;
            this.linkButtonControl2.EventValue = null;
            this.linkButtonControl2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkButtonControl2.LinkUrl = "http://shang.qq.com/wpa/qunwpa?idkey=0eee38260deeec05d8e63f858555e36674809f8728a7" +
    "43b3f34da1e5913d6296";
            this.linkButtonControl2.Location = new System.Drawing.Point(103, 379);
            this.linkButtonControl2.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl2.Name = "linkButtonControl2";
            this.linkButtonControl2.Size = new System.Drawing.Size(225, 26);
            this.linkButtonControl2.TabIndex = 9;
            this.linkButtonControl2.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl1_ButtonClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GrapecityReportsLibrary.Properties.Resources.葡萄城报表微信号;
            this.pictureBox1.Location = new System.Drawing.Point(613, 263);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.label4.Location = new System.Drawing.Point(613, 470);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 42);
            this.label4.TabIndex = 12;
            this.label4.Text = "千万种报表，同一种选择\r\n扫码关注葡萄城报表微信号";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContactUs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 530);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.linkButtonControl2);
            this.Controls.Add(this.linkButtonControl1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ContactUs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "关于行业报表模板库";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Controls.LinkButtonControl linkButtonControl1;
        private Controls.LinkButtonControl linkButtonControl2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
    }
}