﻿namespace GrapecityReportsLibrary.Controls
{
    partial class HomePageControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomePageControl));
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.homeItemControl6 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.homeItemControl5 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.homeItemControl4 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.homeItemControl3 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.homeItemControl2 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.homeItemControl1 = new GrapecityReportsLibrary.Controls.HomeItemControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 128);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1000, 462);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.homeItemControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1000, 462);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // homeItemControl6
            // 
            this.homeItemControl6.BackColor = System.Drawing.Color.White;
            this.homeItemControl6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl6.EventData = null;
            this.homeItemControl6.EventName = null;
            this.homeItemControl6.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl6.FunctionName = "Omni-Channel 全渠道零售 - 总体";
            this.homeItemControl6.Icon = global::GrapecityReportsLibrary.Properties.Resources.Omni_Channel全渠道零售Dashboard;
            this.homeItemControl6.IsNewReport = true;
            this.homeItemControl6.Location = new System.Drawing.Point(676, 241);
            this.homeItemControl6.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl6.Name = "homeItemControl6";
            this.homeItemControl6.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl6.ReportID = "H1402";
            this.homeItemControl6.ReportToolTip = null;
            this.homeItemControl6.Size = new System.Drawing.Size(314, 211);
            this.homeItemControl6.TabIndex = 5;
            this.homeItemControl6.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // homeItemControl5
            // 
            this.homeItemControl5.BackColor = System.Drawing.Color.White;
            this.homeItemControl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl5.EventData = null;
            this.homeItemControl5.EventName = null;
            this.homeItemControl5.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl5.FunctionName = "生产制造 - 生产车间看板 03";
            this.homeItemControl5.Icon = global::GrapecityReportsLibrary.Properties.Resources.生产制造___生产车间看板_03;
            this.homeItemControl5.IsNewReport = true;
            this.homeItemControl5.Location = new System.Drawing.Point(343, 241);
            this.homeItemControl5.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl5.Name = "homeItemControl5";
            this.homeItemControl5.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl5.ReportID = "H0904";
            this.homeItemControl5.ReportToolTip = null;
            this.homeItemControl5.Size = new System.Drawing.Size(313, 211);
            this.homeItemControl5.TabIndex = 4;
            this.homeItemControl5.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // homeItemControl4
            // 
            this.homeItemControl4.BackColor = System.Drawing.Color.White;
            this.homeItemControl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl4.EventData = null;
            this.homeItemControl4.EventName = null;
            this.homeItemControl4.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl4.FunctionName = "冲压车间大屏";
            this.homeItemControl4.Icon = global::GrapecityReportsLibrary.Properties.Resources.冲压车间大屏;
            this.homeItemControl4.IsNewReport = true;
            this.homeItemControl4.Location = new System.Drawing.Point(10, 241);
            this.homeItemControl4.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl4.Name = "homeItemControl4";
            this.homeItemControl4.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl4.ReportID = "H1504";
            this.homeItemControl4.ReportToolTip = null;
            this.homeItemControl4.Size = new System.Drawing.Size(313, 211);
            this.homeItemControl4.TabIndex = 3;
            this.homeItemControl4.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // homeItemControl3
            // 
            this.homeItemControl3.BackColor = System.Drawing.Color.White;
            this.homeItemControl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl3.EventData = null;
            this.homeItemControl3.EventName = null;
            this.homeItemControl3.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl3.FunctionName = "跨江大桥汽车流量报表";
            this.homeItemControl3.Icon = global::GrapecityReportsLibrary.Properties.Resources.跨江大桥汽车流量报表;
            this.homeItemControl3.IsNewReport = true;
            this.homeItemControl3.Location = new System.Drawing.Point(676, 10);
            this.homeItemControl3.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl3.Name = "homeItemControl3";
            this.homeItemControl3.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl3.ReportID = "H1119";
            this.homeItemControl3.ReportToolTip = null;
            this.homeItemControl3.Size = new System.Drawing.Size(314, 211);
            this.homeItemControl3.TabIndex = 2;
            this.homeItemControl3.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // homeItemControl2
            // 
            this.homeItemControl2.BackColor = System.Drawing.Color.White;
            this.homeItemControl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl2.EventData = null;
            this.homeItemControl2.EventName = null;
            this.homeItemControl2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl2.FunctionName = "仓库 - 运输能力监控";
            this.homeItemControl2.Icon = global::GrapecityReportsLibrary.Properties.Resources.仓库_运输能力监控;
            this.homeItemControl2.IsNewReport = true;
            this.homeItemControl2.Location = new System.Drawing.Point(343, 10);
            this.homeItemControl2.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl2.Name = "homeItemControl2";
            this.homeItemControl2.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl2.ReportID = "H1118";
            this.homeItemControl2.ReportToolTip = null;
            this.homeItemControl2.Size = new System.Drawing.Size(313, 211);
            this.homeItemControl2.TabIndex = 1;
            this.homeItemControl2.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // homeItemControl1
            // 
            this.homeItemControl1.BackColor = System.Drawing.Color.White;
            this.homeItemControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.homeItemControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.homeItemControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homeItemControl1.EventData = null;
            this.homeItemControl1.EventName = null;
            this.homeItemControl1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.homeItemControl1.FunctionName = "模板库使用说明 - 总体";
            this.homeItemControl1.Icon = global::GrapecityReportsLibrary.Properties.Resources.模板库使用简介1;
            this.homeItemControl1.IsNewReport = true;
            this.homeItemControl1.Location = new System.Drawing.Point(10, 10);
            this.homeItemControl1.Margin = new System.Windows.Forms.Padding(10);
            this.homeItemControl1.Name = "homeItemControl1";
            this.homeItemControl1.Padding = new System.Windows.Forms.Padding(10);
            this.homeItemControl1.ReportID = "H0001";
            this.homeItemControl1.ReportToolTip = null;
            this.homeItemControl1.Size = new System.Drawing.Size(313, 211);
            this.homeItemControl1.TabIndex = 0;
            this.homeItemControl1.ReportClick += new GrapecityReportsLibrary.Controls.HomeItemControl.ReportClickEvent(this.homeItemControl1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 128);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(0, 73);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 2, 0, 0);
            this.label3.Size = new System.Drawing.Size(1000, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "您在运行报表时还能打开报表设计界面，可自由修改并查看效果，快速体验葡萄城报表设计器的强大功能";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(0, 50);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 2, 0, 0);
            this.label2.Size = new System.Drawing.Size(1000, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "葡萄城报表模板库内置百余套免费模板，[行业案例]收纳多个行业的实际报表应用，[报表功能]提供报表设计实现细节";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.label1.Size = new System.Drawing.Size(1000, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "欢迎使用葡萄城报表模板库";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 590);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1000, 80);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dateTimePicker1);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.linkLabel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(334, 80);
            this.panel4.TabIndex = 4;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 79);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 21);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(87, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 15);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.Highlight;
            this.linkLabel1.Location = new System.Drawing.Point(16, 25);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(184, 34);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "ActiveReports 官方交流QQ群：\r\n109783140";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Right;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(701, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(10, 2, 0, 0);
            this.label4.Size = new System.Drawing.Size(299, 80);
            this.label4.TabIndex = 3;
            this.label4.Text = "v2019.01.07 \r\nCopyright © 2019 GrapeCity\r\n";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // HomePageControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Name = "HomePageControl";
            this.Size = new System.Drawing.Size(1000, 670);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private HomeItemControl homeItemControl6;
        private HomeItemControl homeItemControl5;
        private HomeItemControl homeItemControl4;
        private HomeItemControl homeItemControl3;
        private HomeItemControl homeItemControl2;
        private HomeItemControl homeItemControl1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
