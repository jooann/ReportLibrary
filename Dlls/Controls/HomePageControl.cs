﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class HomePageControl : GCRLBaseControl
    {
        public delegate void ReportClickEvent(object sender, EventArgs e);

        public event ReportClickEvent ReportClick;

        public HomePageControl()
        {
            InitializeComponent();
        }

        private void homeItemControl1_Click(object sender, EventArgs e)
        {
            if (ReportClick != null)
            {
                ReportClick(sender, e);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://shang.qq.com/wpa/qunwpa?idkey=0eee38260deeec05d8e63f858555e36674809f8728a743b3f34da1e5913d6296");
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://shang.qq.com/wpa/qunwpa?idkey=0eee38260deeec05d8e63f858555e36674809f8728a743b3f34da1e5913d6296");
        }
    }
}
