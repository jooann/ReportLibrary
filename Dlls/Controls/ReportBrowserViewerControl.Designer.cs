﻿namespace GrapecityReportsLibrary.Controls
{
    partial class ReportBrowserViewerControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.reportInfoControl1 = new GrapecityReportsLibrary.Controls.ReportInfoControl();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // reportInfoControl1
            // 
            this.reportInfoControl1.CurrentReport = null;
            this.reportInfoControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.reportInfoControl1.EventData = null;
            this.reportInfoControl1.EventName = null;
            this.reportInfoControl1.Location = new System.Drawing.Point(686, 0);
            this.reportInfoControl1.Name = "reportInfoControl1";
            this.reportInfoControl1.Size = new System.Drawing.Size(270, 630);
            this.reportInfoControl1.TabIndex = 0;
            this.reportInfoControl1.ReportModified += new GrapecityReportsLibrary.Controls.ReportInfoControl.ReportModifiedEvent(this.reportInfoControl1_ReportModified);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(686, 630);
            this.webBrowser1.TabIndex = 1;
            // 
            // ReportBrowserViewerControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.reportInfoControl1);
            this.Name = "ReportBrowserViewerControl";
            this.Size = new System.Drawing.Size(956, 630);
            this.Load += new System.EventHandler(this.ReportViewerControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ReportInfoControl reportInfoControl1;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}
