﻿namespace GrapecityReportsLibrary.Controls
{
    partial class ReportViewerControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.viewer1 = new GrapeCity.ActiveReports.Viewer.Win.Viewer();
            this.reportInfoControl1 = new GrapecityReportsLibrary.Controls.ReportInfoControl();
            this.SuspendLayout();
            // 
            // viewer1
            // 
            this.viewer1.AllowSplitter = false;
            this.viewer1.CurrentPage = 0;
            this.viewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewer1.Location = new System.Drawing.Point(0, 0);
            this.viewer1.Name = "viewer1";
            this.viewer1.PreviewPages = 0;
            // 
            // 
            // 
            // 
            // 
            // 
            this.viewer1.Sidebar.ParametersPanel.ContextMenu = null;
            this.viewer1.Sidebar.ParametersPanel.Text = "参数";
            this.viewer1.Sidebar.ParametersPanel.Width = 200;
            // 
            // 
            // 
            this.viewer1.Sidebar.SearchPanel.ContextMenu = null;
            this.viewer1.Sidebar.SearchPanel.Text = "查找结果";
            this.viewer1.Sidebar.SearchPanel.Width = 200;
            // 
            // 
            // 
            this.viewer1.Sidebar.ThumbnailsPanel.ContextMenu = null;
            this.viewer1.Sidebar.ThumbnailsPanel.Text = "页面缩略图";
            this.viewer1.Sidebar.ThumbnailsPanel.Width = 200;
            this.viewer1.Sidebar.ThumbnailsPanel.Zoom = 0.1D;
            // 
            // 
            // 
            this.viewer1.Sidebar.TocPanel.ContextMenu = null;
            this.viewer1.Sidebar.TocPanel.Expanded = true;
            this.viewer1.Sidebar.TocPanel.Text = "文档结构图";
            this.viewer1.Sidebar.TocPanel.Width = 200;
            this.viewer1.Sidebar.Width = 200;
            this.viewer1.Size = new System.Drawing.Size(686, 630);
            this.viewer1.TabIndex = 1;
            // 
            // reportInfoControl1
            // 
            this.reportInfoControl1.CurrentReport = null;
            this.reportInfoControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.reportInfoControl1.EventData = null;
            this.reportInfoControl1.EventName = null;
            this.reportInfoControl1.Location = new System.Drawing.Point(686, 0);
            this.reportInfoControl1.Name = "reportInfoControl1";
            this.reportInfoControl1.Size = new System.Drawing.Size(270, 630);
            this.reportInfoControl1.TabIndex = 0;
            this.reportInfoControl1.ReportModified += new GrapecityReportsLibrary.Controls.ReportInfoControl.ReportModifiedEvent(this.reportInfoControl1_ReportModified);
            // 
            // ReportViewerControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.viewer1);
            this.Controls.Add(this.reportInfoControl1);
            this.Name = "ReportViewerControl";
            this.Size = new System.Drawing.Size(956, 630);
            this.Load += new System.EventHandler(this.ReportViewerControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ReportInfoControl reportInfoControl1;
        private GrapeCity.ActiveReports.Viewer.Win.Viewer viewer1;
    }
}
