﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class MenuItemControl : GCRLBaseControl
    {
        public delegate void HeaderClickEvent(object sender, EventArgs e);
        public event HeaderClickEvent HeaderClick;

        public delegate void ReportClickEvent(object sender, EventArgs e);
        public event ReportClickEvent ReportClick;

        public Category Category { get; set; }
        

        public MenuHeaderControl Header { get; set; }

        public string HeaderName { get; set; }

        public List<Report> Reports { get; set; }

        [DefaultValue(false)]
        public bool InitialExpand { get; set; }

        public MenuItemControl()
        {
            InitializeComponent();
        }

        private void MenuItem_Load(object sender, EventArgs e)
        {
            MenuHeaderControl header = new MenuHeaderControl();
            
            header.HeaderClick += Header_Click;
            header.Category = this.Category;
            this.panel1.Controls.Add(header);
            foreach (Report report in Reports)
            {
                MenuChildControl child = new MenuChildControl();                
                child.ReportClick += Child_ReportClick;
                child.Report = report;
                this.flowLayoutPanel1.Controls.Add(child);
            }
            if (InitialExpand)
            {
                this.Height = 36 + Reports.Count * 36;
            }
            else
            {
                this.Height = 36;
            }
            this.Header = header;
        }

        private void Child_ReportClick(object sender, EventArgs e)
        {
            if (ReportClick != null)
            {
                ReportClick(sender, e);
            }
        }

        private void Header_Click(object sender, EventArgs e)
        {
            if (HeaderClick != null)
            {
                HeaderClick(this, e);
            }            
        }
    }
}
