﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class GCRLBaseControl : UserControl
    {
        public string EventName { get; set; }
        public string EventData { get; set; }

        public GCRLBaseControl()
        {
            InitializeComponent();
        }
    }
}
