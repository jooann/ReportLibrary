﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using GrapeCity.ActiveReports;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ReportInfoControl : GCRLBaseControl
    {

        public delegate void ReportModifiedEvent(object sender, EventArgs e);

        public event ReportModifiedEvent ReportModified;

        string _reportName, _dbName;
        private Report _report;
        public Report CurrentReport
        {
            get
            {
                return _report;
            }
            set
            {
                this._report = value;
                if (value != null)
                {
                    if (string.IsNullOrEmpty(this._report.WebUrl))
                    {
                        this.reportDescriptionControl1.webBrowser1.DocumentText = string.Format("<html><head><link href='http://demo.gcpowertools.com.cn/ActiveReports/ASPNET/ControlExplorer/css/style.css' rel='stylesheet' type='text/css'></head><body><div class='reportintroduce'>{0}</div></body></html>", CurrentReport.Description);
                    }
                    else
                    {
                        string webUrlHtml = string.Format("<ul><li><div class=\"li-header\">在线查看此报表</div><div><a target=\"blank\" href=\"{0}\" >Web 版本演示地址</a></div></li></ul>", this._report.WebUrl.Substring(1));
                        this.reportDescriptionControl1.webBrowser1.DocumentText = string.Format("<html><head><link href='http://demo.gcpowertools.com.cn/ActiveReports/ASPNET/ControlExplorer/css/style.css' rel='stylesheet' type='text/css'></head><body><div class='reportintroduce'>{0}{1}</div></body></html>", webUrlHtml, CurrentReport.Description);
                    }
                    //笨办法固定显示资源按钮
                    if (string.IsNullOrEmpty(this._report.ReferURL))
                    {
                        panel15.Height = 108;
                    }
                    else
                    {
                        panel15.Height = 178;
                    }
                }
            }
        }
        public ReportInfoControl()
        {
            InitializeComponent();
        }

        //显示或者隐藏右边区域
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (this.Width != 10)
            {
                this.Width = 10;
                this.pictureBox1.Image = GrapecityReportsLibrary.Properties.Resources.Open;
            }
            else
            {
                this.Width = 270;
                this.pictureBox1.Image = GrapecityReportsLibrary.Properties.Resources.Close;
            }
        }

        // 打开报表设计器
        private void buttonControl1_Click(object sender, EventArgs e)
        {
            EndUserDesigner designer = new EndUserDesigner(this.CurrentReport);
            if (designer.ShowDialog() == DialogResult.OK)
            {
                if (ReportModified != null)
                {
                    ReportModified(sender, e);
                }
            }
        }

        // 打开报表学习资料
        private void buttonControl2_Click(object sender, EventArgs e)
        {
            ReportStudyForm frmStudy = new ReportStudyForm();
            frmStudy.CurrentReport = this.CurrentReport;
            frmStudy.Show();
        }

        private void buttonControl3_ButtonClick(object sender, EventArgs e)
        {
            CustomerFeedback feedback = new CustomerFeedback();

            if (feedback.ShowDialog() == DialogResult.OK)
            {

            }
        }

        // 导出当前报表模板
        private void buttonControl4_Click(object sender, EventArgs e)
        {
            string filename = string.Format(@"{0}\{1}\{2}\{3}\{4}", Application.StartupPath, "Reports", _report.SystemCategory, _report.FunctionCateory, _report.Path);
            //Get DB Path
            _dbName = string.Format(@"{0}\{1}\{2}", Application.StartupPath, "Data", "ArsDemo.db");

            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.Description = "请选择模板文件存放目录";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    string path = dlg.SelectedPath;
                    //Get User SaveAs Path
                    DirectoryInfo saveFolder = new DirectoryInfo(path);
                    if (!saveFolder.Exists)
                    {
                        saveFolder.Create();
                    }

                    //Get DataBase Path
                    FileInfo _db = new FileInfo(_dbName);
                    string dbFileName = _db.Name;
                    string newDBPath = Path.Combine(path, dbFileName);
                    _db.CopyTo(newDBPath, true);

                    //Get Report Template Path
                    FileInfo _reportFile = new FileInfo(filename);
                    string reportFileName = _reportFile.Name;
                    string newReportPath = Path.Combine(path, reportFileName);
                    _reportFile.CopyTo(newReportPath, true);

                    var _frep = new FileInfo(newReportPath);
                    var _srep = new PageReport(_frep);
                    try
                    {
                        _srep.Report.DataSources[0].ConnectionProperties.ConnectString = "DRIVER=SQLITE3 ODBC DRIVER;DATABASE=" + newDBPath;
                        _srep.Save(_frep);
                    }
                    catch (Exception ex)
                    { }

                    bool isInstalled = JudgeARInstalled();

                    if (isInstalled)
                    {
                        MessageBox.Show("报表模板导出成功 ");
                    }
                    else
                    {
                        if (MessageBox.Show("该报表模板由 ActiveReports 报表工具制作，为更好的使用该模板交互功能，请前往官网下载最新版本的 ActiveReports ！ ", "导出报表模板完成", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                        {
                            System.Diagnostics.Process.Start("https://www.grapecity.com.cn/download?pid=16");
                        }
                    }
                }
            }
        }

        private bool JudgeARInstalled()
        {
            Microsoft.Win32.RegistryKey uninstallNode = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
            foreach (string subKeyName in uninstallNode.GetSubKeyNames())
            {
                Microsoft.Win32.RegistryKey subKey = uninstallNode.OpenSubKey(subKeyName);
                object displayName = subKey.GetValue("DisplayName");
                if (displayName != null)
                {
                    if (displayName.ToString().Contains("ActiveReports"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
