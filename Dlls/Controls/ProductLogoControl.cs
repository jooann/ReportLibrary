﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ProductLogoControl : GCRLBaseControl
    {

        public delegate void ProductLogoClickEvent(object sender, EventArgs e);

        public event ProductLogoClickEvent ProductLogoClick;

        public ProductLogoControl()
        {
            InitializeComponent();
        }

        private void ProductLogoControl_Click(object sender, EventArgs e)
        {
            if (ProductLogoClick != null)
            {
                //CommonFunctions.AddARDemoOperation(EventTypes.点击左上角Logo,null);
                ProductLogoClick(sender, e);
            }
        }
    }
}
