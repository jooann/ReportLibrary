﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class MenuChildControl : GCRLBaseControl
    {

        public delegate void ReportClickEvent(object sender, EventArgs e);

        public event ReportClickEvent ReportClick;

        public Report Report { get; set; }
        
        public bool IsSelected { get; set; }
        public MenuChildControl()
        {
            InitializeComponent();
        }

        private void MenuChild_Load(object sender, EventArgs e)
        {
            // 设置默认背景色
            this.BackColor = Colors.ReportNormalColor;
            this.label1.Text = this.Report.Name;
            this.toolTip1.SetToolTip(this.label1, this.label1.Text);
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            // 如果是选中的报表，不改变背景色，直接退出
            if (IsSelected)
            {
                return;
            }

            // 设置鼠标经过时的颜色
            this.BackColor = Colors.ReportHoverColor;
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            // 如果是选中的报表，不改变背景色，直接退出
            if (IsSelected)
            {
                return;
            }

            // 设置鼠标经过时的颜色
            this.BackColor = Colors.ReportNormalColor;
        }        

        private void label1_Click(object sender, EventArgs e)
        {
            if (ReportClick != null)
            {
                //CommonFunctions.AddARDemoOperation(EventTypes.查看报表, this.Report.Name);
                ReportClick(this, e);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("收藏成功！");
        }
    }
}
