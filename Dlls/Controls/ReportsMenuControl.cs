﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ReportsMenuControl : GCRLBaseControl
    {
        public FlowLayoutPanel flpIndustryMenu;
        public FlowLayoutPanel flpFunctionMenu;

        public string SelectedReport { get; set; }

        public delegate void MenuItemClickEvent(object sender, EventArgs e);
        

        public event MenuItemClickEvent ReportClick;

        public event MenuItemClickEvent CategoryClick;

        public event MenuItemClickEvent SeriesClick;

        public List<MenuItem> ReportMenu { get; set; }

        public ReportsMenuControl()
        {
            InitializeComponent();
        }

        // 当前选中的报表分类类型，行业案例或者报表功能
        private string currentType = null;

        //上一个选中的分类项目
        private MenuItemControl originaIndustrylmenu = null;
        private MenuItemControl originalFunctionmenu = null;

        // 上一个选中的报表项目
        private MenuChildControl originalreport = null;

        private void ReportsMenu_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }

        }

        private void Item1_ReportClick(object sender, EventArgs e)
        {
            MenuChildControl item = (sender as MenuChildControl);

            // 第一次选择报表
            if (originalreport == null)
            {
                originalreport = item;
            }

            // 第一次之后选择报表
            if (originalreport.Report.ID != item.Report.ID)
            {
                // 记录当前选中报表状态
                originalreport.IsSelected = false;
                originalreport.BackColor = Colors.ReportNormalColor;
                (originalreport.Parent.Parent as MenuItemControl).Header.BackColor = Colors.HeaderNormalColor;
                originalreport = item;
            }

            // 修改当前选中报表的设置
            item.IsSelected = true;
            item.BackColor = Colors.ReportSelectColor;
            (item.Parent.Parent as MenuItemControl).Header.BackColor = Colors.HeaderSelectedColor;
            this.currentType = item.Report.SystemCategory;

            if (ReportClick != null)
            {
                ReportClick(sender, e);
            }
        }

        private void Item1_HeaderClick(object sender, EventArgs e)
        {
            MenuItemControl item = (sender as MenuItemControl);
            if (item.Category.SystemCategory == "行业案例")
            {
                if (originaIndustrylmenu == null)
                {
                    originaIndustrylmenu = item;
                    item.Height = 38 + item.Reports.Count * 33;
                }
                else if (originaIndustrylmenu.Category.Name == item.Category.Name)
                {
                    item.Height = 36;
                    originaIndustrylmenu = null;
                }
                else
                {
                    originaIndustrylmenu.Height = 36;
                    item.Height = 38 + item.Reports.Count * 33;
                    originaIndustrylmenu = item;
                }
            }
            else
            {
                if (originalFunctionmenu == null)
                {
                    originalFunctionmenu = item;
                    item.Height = 38 + item.Reports.Count * 33;
                }
                else if (originalFunctionmenu.Category.Name == item.Category.Name)
                {
                    item.Height = 36;
                    originalFunctionmenu = null;
                }
                else
                {
                    originalFunctionmenu.Height = 36;
                    item.Height = 38 + item.Reports.Count * 33;
                    originalFunctionmenu = item;
                }
            }

            if (CategoryClick != null)
            {
                CategoryClick(sender, e);
            }
        }
        

        public void BuildMenu(string type, List<MenuItem> menu)
        {
            if (type == "行业案例")
            {
                if (flpIndustryMenu == null)
                {
                    flpIndustryMenu = new FlowLayoutPanel();
                    flpIndustryMenu.Dock = DockStyle.Fill;
                    flpIndustryMenu.AutoScroll = true;
                    foreach (MenuItem menuitem in menu)
                    {
                        MenuItemControl item = new MenuItemControl();
                        item.HeaderClick += Item1_HeaderClick;
                        item.ReportClick += Item1_ReportClick;
                        item.Category = menuitem.Category;
                        item.Reports = menuitem.Reports;
                        this.flpIndustryMenu.Controls.Add(item);
                    }
                    this.panel3.Controls.Add(flpIndustryMenu);
                }
                flpIndustryMenu.BringToFront();
            }
            else
            {
                if (flpFunctionMenu == null)
                {
                    flpFunctionMenu = new FlowLayoutPanel();
                    flpFunctionMenu.Dock = DockStyle.Fill;
                    flpFunctionMenu.AutoScroll = true;
                    flpFunctionMenu.HorizontalScroll.Enabled = false;
                    flpFunctionMenu.HorizontalScroll.Visible = false;
                    foreach (MenuItem menuitem in menu)
                    {
                        MenuItemControl item = new MenuItemControl();
                        item.HeaderClick += Item1_HeaderClick;
                        item.ReportClick += Item1_ReportClick;
                        item.Category = menuitem.Category;
                        item.Reports = menuitem.Reports;
                        this.flpFunctionMenu.Controls.Add(item);
                    }
                    this.panel3.Controls.Add(flpFunctionMenu);
                }
                flpFunctionMenu.BringToFront();
            }
        }

        public void ShowReportMenu(string type)
        {
            var xx = this.ParentForm;
            
            //Add the new homepage to show.
            if (type == "行业案例")
            {
                this.flpIndustryMenu.BringToFront();
            }
            else
            {
                this.flpFunctionMenu.BringToFront();
            }
        }
        public void SelectReport(Report report)
        {
            foreach (MenuItemControl item in this.flpIndustryMenu.Controls)
            {
                if (item.Category.Name == report.FunctionCateory)
                {
                    foreach (MenuChildControl child in item.flowLayoutPanel1.Controls)
                    {
                        if (child.Report.ID == report.ID)
                        {
                            this.Item1_HeaderClick(item, null);

                            this.Item1_ReportClick(child, null);

                            return;
                        }
                    }
                }
            }
        }

        private void buttonControl1_ButtonClick(object sender, EventArgs e)
        {
            string type = (sender as LinkButtonControl).ButtonText;

            if (type == "行业案例")
            {
                btnIndustry.ButtonBackColor = Color.FromArgb(83, 66, 98);
                btnFunction.ButtonBackColor = Color.Transparent;
            }
            else
            {
                btnFunction.ButtonBackColor = Color.FromArgb(83, 66, 98);
                btnIndustry.ButtonBackColor = Color.Transparent;
            }
            //CommonFunctions.AddARDemoOperation(EventTypes.切换报表菜单, type);
            ShowReportMenu(type);
            SeriesClick(sender, e);
        }
    }
}
