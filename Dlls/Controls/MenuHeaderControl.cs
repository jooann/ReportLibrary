﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class MenuHeaderControl : GCRLBaseControl
    {
        public delegate void HeaderClickEvent(object sender, EventArgs e);

        public event HeaderClickEvent HeaderClick;

        public Category Category { get; set; }        
        
        public MenuHeaderControl()
        {
            InitializeComponent();
        }

        private void MenuChild_Load(object sender, EventArgs e)
        {
            this.BackColor = Colors.HeaderNormalColor;
            this.label1.Text = this.Category.Name;
            if (System.IO.File.Exists(string.Format(@"Images\分类图片\{0}.png", this.Category.Icon)))
            {
                this.pictureBox1.Image = Image.FromFile(string.Format(@"Images\分类图片\{0}.png", this.Category.Icon));
            }
            else
            {
                this.pictureBox1.Image = Image.FromFile(string.Format(@"Images\分类图片\{0}.png", "报表分类"));
            }

        }        
        
        private void label1_Click(object sender, EventArgs e)
        {                      
            if (HeaderClick != null)
            {
                HeaderClick(this, e);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (HeaderClick != null)
            {
                HeaderClick(this, e);
            }
        }
    }
}
