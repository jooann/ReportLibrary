﻿using GrapecityReportsLibrary.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary
{
    public partial class ContactUs : Form
    {
        public ContactUs()
        {
            InitializeComponent();
        }

        private void linkButtonControl1_ButtonClick(object sender, EventArgs e)
        {
            LinkButtonControl btnUrl = sender as LinkButtonControl;
         
            System.Diagnostics.Process.Start(btnUrl.LinkUrl);
        }

        private void linkButtonControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
