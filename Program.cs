﻿using Grapecity.PowerTools.Api.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;

namespace GrapecityReportsLibrary
{

    static class Program
    {
        private static Mutex mutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            // 系统以中文语言运行
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("zh-CN");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CN");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new Main());


            //// 检查是否已经打开报表模板库
            //mutex = new Mutex(false, "葡萄城报表模板库");

            //if (!mutex.WaitOne(0, false))
            //{
            //    MessageBox.Show("您已经打开葡萄城报表模板库，赶紧去使用吧！","葡萄城报表模板库",MessageBoxButtons.OK,MessageBoxIcon.Information);
            //    return;
            //}

            //// 全局异常处理
            //Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            //Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            //Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            //AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            //// 系统以中文语言运行
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("zh-CN");
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CN");

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);


            //// 检测当前用户是否已经获得正确的客户标识
            ////CommonFunctions.PrepareCustomerEnvironment();

            //// 第一次安装报表模板库，安装完成打开活动页面，这个地址以后需要替换为报表模板库的介绍页面
            //if (CommonFunctions.CheckIsFirstInstalled())
            //{                
            //    System.Diagnostics.Process.Start("http://www.grapecity.com.cn/enterprise-solutions/activereports_server/activity/?utm_source=Demo&utm_medium=ActiveReportsServer&utm_term=GrapecityReportsLibrary&utm_content=ARSLibraryInstalled");
            //}

            //Application.Run(new Main());
        }


        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            G_Global.CallWebApi("ERROR", "Application_ApplicationExit", "", "");

            MessageBox.Show("天啦，我也不知道什么操作引起这个问题，看样子你需要重启程序了！！！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            G_Global.CallWebApi("ERROR", "CurrentDomain_UnhandledException", "", "");
            MessageBox.Show("天啦，我也不知道什么操作引起这个问题，看样子你需要重启程序了！！！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            G_Global.CallWebApi("ERROR", "Application_ThreadException", e.Exception.Message, "");
            MessageBox.Show("天啦，我也不知道什么操作引起这个问题，看样子你需要重启程序了！！！","系统提示",MessageBoxButtons.OK,MessageBoxIcon.Warning);
        }
    }
    public static class GlobalData
    {
        public static Guid UserID = Guid.Empty;
    }
}
