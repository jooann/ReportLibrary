﻿using GrapecityReportsLibrary.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary
{
    public partial class ReportStudyForm : Form
    {
        public Report CurrentReport { get; set; }

        public ReportStudyForm()
        {
            InitializeComponent();
        }

        private void ReportStudyForm_Load(object sender, EventArgs e)
        {
            //CommonFunctions.AddARDemoOperation(EventTypes.开始查看教程, this.CurrentReport.Name);

            this.Text = this.CurrentReport.Name;
            this.webBrowser1.Url = new Uri(this.CurrentReport.ReferURL);
        }

        private void ReportStudyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //CommonFunctions.AddARDemoOperation(EventTypes.结束查看教程, this.CurrentReport.Name);
        }
    }
}
